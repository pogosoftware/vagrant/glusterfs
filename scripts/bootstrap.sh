#!/bin/bash

echo "[TASK 1] Update /etc/hosts file"
cat >>/etc/hosts<<EOF
192.168.33.21   gluster1.vagrant.local  gluster1
192.168.33.22   gluster2.vagrant.local  gluster2
192.168.33.23   gluster3.vagrant.local  gluster3
EOF

echo "[TASK 2] Install GlusterFS"
apt-get install -qq -y software-properties-common >/dev/null 2>&1
add-apt-repository --yes ppa:gluster/glusterfs-7 >/dev/null 2>&1
apt-get update -qq >/dev/null 2>&1
apt-get install -qq -y glusterfs-server >/dev/null 2>&1

systemctl enable glusterd.service >/dev/null 2>&1
systemctl start glusterd.service